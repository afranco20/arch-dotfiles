
set nocompatible              " be iMproved, required
filetype off                  " required

source ~/.vim/vimrc/plugins.vim
source ~/.vim/vimrc/plugins_config.vim
source ~/.vim/vimrc/color.vim
source ~/.vim/vimrc/basic.vim
source ~/.vim/vimrc/settings.vim
source ~/.vim/vimrc/files.vim
" source ~/.vim/vimrc/functions.vim

if has('nvim')
  source ~/.vim/vimrc/nvim.vim
endif
