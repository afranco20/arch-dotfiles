" Fast escape
map <c-c> <Esc>

" :w!!
" write the file when you accidentally opened it without the right root priveleges
cmap w!! w !sudo tee % > /dev/null

" highlight the current line
set cursorline

" Use the OS clipboard
set clipboard+=unnamedplus

"This is a nice buffer switcher
" :nnoremap <F5> :buffers<CR>:buffer<Space>

" Insert date and time
iab xdate <c-r>=strftime("%a %b %d %H:%M:%S %Y %Z")<cr>

" Mouse Enabled
set mouse=a

" Folding
" set foldmethod=syntax
" set foldcolumn=1

" Splits
nnoremap <F1> :vsplit<CR>
nnoremap <F2> :split<CR>
nnoremap <F3> :close<CR>

" Refresh/Reload vim script
nnoremap <M-r> :so %<CR>

" Disable modeline
set noshowmode

" set terminal title
set title

