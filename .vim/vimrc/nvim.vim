:tnoremap <Esc> <C-\><C-n>
:tmap <C-c> <Esc>

" let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 1

" Set python3 provider
let g:python3_host_prog = '/usr/bin/python3'

" Disable python2 and ruby
let g:loaded_python_provider = 1
let g:loaded_ruby_provider = 1

let g:deoplete#enable_at_startup = 1
