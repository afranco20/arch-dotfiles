
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors [Toggles between light and dark theme]
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <leader>, :call Theme()<cr>

" let s:enabled = 0

function! Theme()
  " if s:enabled == 1
    " colorscheme onedark | AirlineTheme onedark
    " let s:enabled = 0
  " else
    " colorscheme one | AirlineTheme one
    " let s:enabled = 1
  " endif

  let hour = strftime("%H")
  if 6 <= hour && hour < 18
    colorscheme one | AirlineTheme one
  else
    colorscheme onedark | AirlineTheme onedark
  endif
endfunction
