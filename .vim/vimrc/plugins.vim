
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim-Plug
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.vim/plugins')

""""""""""""""""""""""""""""""
" => Color Schemes
""""""""""""""""""""""""""""""
" Molokai
" Plug 'tomasr/molokai'

" Quantum
" Plug 'tyrannicaltoucan/vim-quantum'

" 256  term Onedark
Plug 'joshdick/onedark.vim'
Plug 'rakr/vim-one'

" Rainbow Parentheses
Plug 'junegunn/rainbow_parentheses.vim'


""""""""""""""""""""""""""""""
" => Syntax
""""""""""""""""""""""""""""""
" C Highlighting
Plug 'NLKNguyen/c-syntax.vim', { 'for': 'c' }

" Rust Highlighting
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

""""""""""""""""""""""""""""""
" => UI Enhancements
""""""""""""""""""""""""""""""
" Denite
" Plug 'Shougo/denite.nvim'

" Startpage
Plug 'mhinz/vim-startify'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" NERDTree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" Ctrl-P
Plug 'kien/ctrlp.vim', { 'do': 'g:ctrlp_map' }

" Vim Search-Index
Plug 'google/vim-searchindex'

" Traling Whitespace
Plug 'bronson/vim-trailing-whitespace'

" Github
Plug 'tpope/vim-fugitive'
" Plug 'tpope/vim-fugitive', { 'on': [ 'Git', 'Gstatus', 'Gwrite', 'Glog', 'Gcommit', 'Gblame', 'Ggrep', 'Gdiff', ] }

" Tagbar
Plug 'majutsushi/tagbar'

" Tmux-Navigator
Plug 'christoomey/vim-tmux-navigator'

" Easy Motion
Plug 'easymotion/vim-easymotion'


""""""""""""""""""""""""""""""
" => Editing Enhancements
""""""""""""""""""""""""""""""
" Editor Config
" Plug 'editorconfig/editorconfig-vim'

" ALE [Asynchronus Syntastic]
Plug 'w0rp/ale'

" Deocomplete [Asynchronus YouCompleteMe]
Plug 'Shougo/deoplete.nvim'

" UltiSnips
Plug 'sirver/ultisnips', { 'on': [] }

augroup load_ultisnips
  autocmd!
  autocmd InsertEnter * call plug#load('ultisnips') | autocmd! load_ultisnips
augroup END

" Vim-Snippets
Plug 'honza/vim-snippets'

" NERDCommenter
Plug 'scrooloose/nerdcommenter'

" Auto-Pairs
Plug 'jiangmiao/auto-pairs'

" Surround.vim
Plug 'tpope/vim-surround'

" Tabular
Plug 'godlygeek/tabular', { 'on': 'Tabularize' }

" Rename
Plug 'danro/rename.vim'

" Syntastic
" Plug 'scrooloose/syntastic'

Plug 'rhysd/vim-clang-format'

call plug#end()
