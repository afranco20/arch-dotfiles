" Enable syntax highlighting
syntax enable

" 256 Color
set t_Co=256

" True Color
set  termguicolors

" 256 Color
let g:onedark_termcolors = 256

" Italics
let g:onedark_terminal_italics=1

" Colorscheme
set background=dark        " for the light version
colorscheme onedark

" Statusbar/tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='onedark'
" let g:airline_powerline_fonts = 1

" Rainbow Parentheses
let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]
" let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]
au VimEnter * RainbowParentheses

augroup rainbow_html
  autocmd!
  autocmd FileType html,scheme RainbowParentheses!
augroup END
