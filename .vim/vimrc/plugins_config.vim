
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Snippets
let g:UltiSnipsSnippetsDir="~/.vim/bundle/vim-snippets/snippets/"

" syntax check
" let g:syntastic_c_checkers = ['gcc']
" let g:syntastic_c_compiler_options = "-Wall -Wextra -Wpedantic -Wunreachable-code -Wshadow"

" C linter
let g:ale_c_gcc_options = "-Wall -Wextra -Wpedantic -Wunreachable-code -Wshadow"

" Rust linter
" let g:ale_rust_rls_executable = "rls"

" Java linter
" let g:ale_java_javac_classpath
" let g:ale_java_javac_options

" Ctrl-P
let g:ctrlp_map = '<c-p>'
" map <c-p> :CtrlP<CR>
" map <C-b> :CtrlPBuffer<CR>
map <F5> :CtrlPBuffer<CR>

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" NERD_Commenter
let g:NERDSpaceDelims = 1
let g:NERDAltDelims_c = 1

" Clang Formatter
" let g:clang_format#code_style = "google"

" Tagbar
nmap <F8> :TagbarToggle<CR>
